package com.example;

import java.util.Objects;

public class ShipBody extends GameObj{

    public static int total_deaths = 0;
    public Type type = Type.Ship;
    public int x_pos;
    public int y_pos;
    public int civilians = 20;
    public int black_box_health = 21;
    public ShipBody(int _xpos, int _ypos){
        x_pos = _xpos;
        y_pos = _ypos;
    }
    public ShipBody(int _xpos, int _ypos, int civilians){
        x_pos = _xpos;
        y_pos = _ypos;
        this.civilians = civilians;
    }
    @Override
    public void count() {
       if(this.type==Type.Ship) {
        this.civilians -=1;
        total_deaths+=1;
       } else if (this.type==Type.Wrek){
        this.black_box_health -=1;
       }
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.x_pos, this.y_pos, this.civilians, this.type);
    }
    @Override
    public boolean equals(Object obj) {
        // defines how 2 ships are equal 
        ShipBody s2 = (ShipBody) obj;
        return (
            this.x_pos==s2.x_pos &&
            this.y_pos==s2.y_pos &&
            this.civilians==s2.civilians &&
            this.type.equals(s2.type) 
        );
    }
    @Override
    public String toString() {
       if(this.type==Type.Ship) {
            return String.format("Ship(%s)", this.civilians);
       } else {
            return String.format("wreck(%s)", this.black_box_health);
       }
    }
    public int take_civilians(int agent_remaining_capacity, int max_capacity) {
        /* changes the current ship civilians as well as return the agents new capacity */
        if(civilians>0){
            int number_of_ppl_agent_can_take = agent_remaining_capacity;
            if(number_of_ppl_agent_can_take>=civilians){
                // if the agent can take more than the civilians on board
                int new_capacity = number_of_ppl_agent_can_take - civilians;
                this.civilians = 0; // no more civilians 
                this.type = Type.Wrek;
                return new_capacity;
            } else {
                // if the agent will not have any more space after taking ppl here 
                this.civilians = civilians - number_of_ppl_agent_can_take;
                int new_capacity = 0; // the agent has not more capacity
                return new_capacity;
            }
            
        } else {
            System.out.println("tried to take civilians from an empty ship");
            return agent_remaining_capacity; // cuz there is no one to take from this ship
        }


       
    }
   
}
