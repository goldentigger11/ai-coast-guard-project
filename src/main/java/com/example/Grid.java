package com.example;

import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Grid {
    GameObj[][] grid;
    private ArrayList<int[]> free_posions = new ArrayList();
    private Random rand = new Random();

    public Grid(int width, int height) {

        // creates an empty grid of only sea types
        grid = new GameObj[height][width];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                grid[i][j] = new Sea();
                int[] pos = { i, j };
                free_posions.add(pos);
            }
        }
    }

    public void addGameObjRandomly(GameObj obj) throws Exception {
        if (free_posions.isEmpty()) {
            throw new Exception("No more free spaces to add object to grid.IE: the grid is full");
        }
        int[] random_free_pos = this.free_posions.remove(this.rand.nextInt(this.free_posions.size()));
        this.grid[random_free_pos[0]][random_free_pos[1]] = obj;

    }

    public String addGameObjRnadomlyAndReturnString(GameObj obj) throws Exception {
        // ! only works on ships and stations
        if (free_posions.isEmpty()) {
            throw new Exception("No more free spaces to add object to grid.IE: the grid is full");
        }
        int[] random_free_pos = this.free_posions.remove(this.rand.nextInt(this.free_posions.size()));
        this.grid[random_free_pos[0]][random_free_pos[1]] = obj;

        int randomCapacity = 1 + rand.nextInt(99);

        if (obj instanceof ShipBody) {
            return "" + random_free_pos[0] + "," + random_free_pos[1] + "," + randomCapacity + ";";
        }
        return "" + random_free_pos[0] + "," + random_free_pos[1] + ";";
    }

    public Grid(State state, int width, int height) {
        // This funciton takes a state and maps it to a grid to allow from prints and so
        // on

        this(width, height);

        // add agent postion

        // add each ship
        for (ShipBody ship : state.ships) {
            grid[ship.y_pos][ship.x_pos] = ship;
        }

        for (ShipBody wreck : state.wrecks) {
            grid[wreck.y_pos][wreck.x_pos] = wreck;
        }

        for (Station station : state.stations) {
            grid[station.y_pos][station.x_pos] = station;
        }

        Guard agent = new Guard();
        agent.x_pos = state.x_pos;
        agent.y_pos = state.y_pos;
        agent.capacity = state.capacity;

        grid[state.y_pos][state.x_pos] = agent;
    }

    public void Print() {
        int max_width_per_element_printed = 10;
        String formater = "%" + max_width_per_element_printed + "s'";
        String str = "";
        String[] arr = { "batee5", "toot", "l", "ronaldooo" };
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {

                System.out.printf(formater, grid[i][j]);
            }
            System.out.println("");
        }
        System.out.println("--------------------------------------------------");
    }

    public static void show_gameplay(Node choosen_node) {
        Node prv = choosen_node;
        ArrayList<Node> path = new ArrayList<>();
        while (prv != null) {

            path.add(0, prv);
            prv = prv.prv_node;
        }

        int width = Node.gird_width;
        int height = Node.grid_height;
        // now I will print them like a gameplay
        int move = 0;
        for (Node node : path) {
            move++;
            System.out.println("action: " + node.prv_action + " " + move);
            Grid g = new Grid(node.state, width, height);
            g.Print();
        }

        //
        System.out.println("cost: " + choosen_node.path_cost);
        System.out.println("blk box: " + choosen_node.state.black_boxes);
        System.out.println("ppl saved: " + choosen_node.state.saved_ppl);
    }

    public static String genGrid() {
        // Choose random variabels
        int max_size = 4;
        Random rand = new Random();
        int width = rand.nextInt(max_size) + 2;
        int height = rand.nextInt(max_size) + 2;
        int capacity = rand.nextInt(50) + 1;
        int x_pos = rand.nextInt(height);
        int y_pos = rand.nextInt(width);
        int stations = rand.nextInt(width*height / 2 - 1) + 1;
        int ships = rand.nextInt(width*height / 2 - 1) + 1;
        // Here

        ArrayList<int[]> free_posions = new ArrayList();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int[] pos = { i, j };
                free_posions.add(pos);
            }
        }
        int[] guard_pos = { x_pos, y_pos };

        free_posions.remove(guard_pos);

        String ans = String.format("%d,%d;%d;", width, height, capacity);
        ans += String.format("%d,%d;", x_pos, y_pos);
        for (int i = 0; i < stations; i++) {
            int[] random_pos = free_posions.remove(rand.nextInt(free_posions.size()));

            ans += String.format("%d,%d,", random_pos[1], random_pos[0]);
        }
        ans = ans.substring(0, ans.length() - 1);
        ans += ";";

        for (int i = 0; i < ships; i++) {
            int[] random_pos = free_posions.remove(rand.nextInt(free_posions.size()));
            int random_capacity = rand.nextInt(99) + 1;
            ans += String.format("%d,%d,%d,", random_pos[1], random_pos[0], random_capacity);
        }

        ans = ans.substring(0, ans.length() - 1);
        ans += ";";

        return ans;
    }

    public static void main(String[] args) throws Exception {
        while (true) {
            String grid = Grid.genGrid();
            System.out.println(grid);
            System.out.println(CoastGuard.solve(grid, "AS2", false));
            Grid.show_gameplay(CoastGuard.final_node);
        }

    }

}
