package com.example;

import java.util.ArrayList;
import java.util.HashMap;

class Checker {

    byte a;
    byte b;
    HashMap<String, Byte> ss = new HashMap<String, Byte>();
    ArrayList<String> is = new ArrayList<String>();
    byte s;
    byte r;
    int d;  /* make this int aboos edak 
            ! CUZ DFS IS STUPID SOMETIMES IT LOOPS SO EVERYONE ON SET DIES BUT CUZ THE NUMBER IS BIGGER THAN 128 
            ! 🤬 THE APPLY PLAN SAYS THE NUMBER OF DEATHS IS -VE */
    byte x00;
    byte x01;
    byte xc;
    byte cp;

    public Checker(byte m, byte n, byte x, byte x00, byte x01, ArrayList<String> st, HashMap<String, Byte> sh) {
        this.a = m;
        this.b = n;
        this.xc = x;
        this.x00 = x00;
        this.x01 = x01;
        this.is = st;
        this.ss = sh;

    }

    boolean f1(int z, int k) {
        if (!f99(x00 + z, x01 + k)) {
            mn();
            return false;
        }

        this.x00 += z;
        this.x01 += k;
        mn();
        return true;
    }

    boolean f2() {
        if (!ss.containsKey(x00 + "," + x01)) {
            mn();
            return false;
        }
        byte ts = ss.get(x00 + "," + x01); // value of ppl in ship
        byte cc = (byte) (xc - cp); // what I can carry at the moment
        if (cc >= ts) { // I can carry more than the ppl here
            cp += ts; // capacity updated with number of ppl here
            ss.replace(x00 + "," + x01, (byte) -20); // edy el black box 100 seconds not 20// ! U forgot to change this to also 20 life points for the black box OPS
        } else {
            cp = xc;
            int n = ts - cc;
            ss.replace(x00 + "," + x01, (byte) n);
        }
        mn();
        return true;
    }

    boolean f3() {

        if (!is.contains(x00 + "," + x01)) {
            mn();
            return false;
        }
        s += cp;
        cp = 0;
        mn();
        return true;
    }

    boolean f4() {

        if (!ss.containsKey(x00 + "," + x01)) {
            mn();
            return false;
        }
        r += 1;
        ss.replace(x00 + "," + x01, (byte) 0);
        mn();
        return true;

    }

    boolean f99(int i, int j) {

        return i >= this.b || i < 0 || j >= this.a || j < 0 ? false : true;

    }

    void mn() {
        ArrayList<String> toclean = new ArrayList<String>();
        for (String k : ss.keySet()) {
            byte v = ss.get(k);
            if (v <= (byte) -1 && v >= (byte) -20)
                v++;
            else {
                if (v == 1) {
                    v = (byte) -20;
                    d++;
                } else {
                    if (v > (byte) 1) {
                        v--;
                        d++;
                    }
                }
            }

            if (v == 0) {
                toclean.add(k);
            } else {
                ss.replace(k, v);
            }

        }
        for (String c : toclean) {
            ss.remove(c);
        }

    }

    void clean() {
        for (String k : ss.keySet()) {
            if (ss.get(k).equals((byte) 0))
                ss.remove(k);
        }
    }

    public boolean cool() {
        return ss.size() == 0 && cp == 0;
    }

    public static boolean applyPlan(String grid, String solution) {
        boolean linkin = true;
        String[] solutionArray = solution.split(";");
        String plan = solutionArray[0];
        int blue = Integer.parseInt(solutionArray[1]);
        int doors = Integer.parseInt(solutionArray[2]);

        plan.replace(" ", "");
        plan.replace("\n", "");
        plan.replace("\r", "");
        plan.replace("\n\r", "");
        plan.replace("\t", "");

        String[] actions = plan.split(",");

        String[] gridArray = grid.split(";");
        String[] dimensions = gridArray[0].split(",");
        byte m = Byte.parseByte(dimensions[0]);
        byte n = Byte.parseByte(dimensions[1]);

        byte x = Byte.parseByte(gridArray[1]);

        String[] xx = gridArray[2].split(",");
        byte x00 = Byte.parseByte(xx[0]);
        byte x01 = Byte.parseByte(xx[1]);

        String[] st = gridArray[3].split(",");
        ArrayList<String> xyz = new ArrayList<String>();
        for (int i = 0; i < st.length - 1; i += 2) {
            xyz.add(st[i] + "," + st[i + 1]);
        }
        //
        String[] sh = gridArray[4].split(",");
        HashMap<String, Byte> m4 = new HashMap<String, Byte>();
        for (int i = 0; i < sh.length - 1; i += 3) {
            m4.put(sh[i] + "," + sh[i + 1], Byte.parseByte(sh[i + 2]));
        }
        Checker s = new Checker(m, n, x, x00, x01, xyz, m4);
        for (int i = 0; i < actions.length; i++) {

            switch (actions[i]) {
                case "up":
                    linkin = s.f1(-1, 0);
                    break;
                case "down":
                    linkin = s.f1(1, 0);
                    break;
                case "right":
                    linkin = s.f1(0, 1);
                    break;
                case "left":
                    linkin = s.f1(0, -1);
                    break;
                case "pickup":
                    linkin = s.f2();
                    break;
                case "drop":
                    linkin = s.f3();
                    break;
                case "retrieve":
                    linkin = s.f4();
                    break;
                default:
                    linkin = false;
                    break;

            }

            if (!linkin){
                System.out.println(actions[i] + " " + i);
                return false;
            }
        }

        boolean t =  s.cool() && s.d == blue && s.r == doors;
        return t;
    }
}
