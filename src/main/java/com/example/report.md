# Report

## Problem Disscution
This is a grid search problem where an agent can take some actions and based on these actions get some rewards. You play as a cost guard trying to save diffrent people from several ships. After saving the ships there are also black boxes to collect. 

## Node
A node is a tuple with < state, prv_node, path_cost>

## Search problem
The search problem is the expansion of states and choosing which state to expand next such that we reach the goals state as quickly and efficently as possible.

## Cost Guard problem
The problem is too choose a sequence of actions that lead your to the best goal state. 

## Functions 
Enque funcitons for each search algorithm
visisted states detection using custom hashes

## Search Algorithms
1. **BFS**: enque at the back
2. **DFS**: enque at the front
3. **ID**: enque at the front but do not enque past max depth then depth+=1
4. **Greedy**: priority queue based on heurstic
5. **A\***: priority queeue based on h(x) + g(x)

## Heurstic 

### Distance to closest ship
The quickest you can win is go to the closest ship and pick up whatever is there then come back. so as long as you can't teleport. you can't win in less steps

### Distance to healthesit ship
The healtheist ship has the most ppl or the best black box. So if you go there and pick everyone up in one go then you still have to go back. and no way you win in less as only 1 person on that ship dies per step. and the ship has more ppl+black box life then steps because the black box alone has 20 health. 

## Comparision
> All are complete as a game has to come to an end even if the guard does nothing. None can loop forever in this context. generaly DFS can loop forever and BFS if the branching factor is getting bigger.

* DFS 
>_Quick, Not optimal, low cup, low ram_

    * execution time: 9
    * Nodes expanded: 112
    * total time: 129
    * average excecution time: 11
    * average nodes: 96
    * average cost: 17148
    * average depth reached: 84
* BFS 
>_slow, not optimal, low cpu, high ram_

    * execution time: 103297
    * Nodes expanded: 2528788
    * total time: 161129
    * average excecution time: 14648
    * average nodes: 398046
    * average cost: 3869
    * average depth reached: 23

* ID 
> _slow, not optimal, low cpu, low ram_

    * execution time: 340
    * Nodes expanded: 39672
    * total time: 11437
    * average excecution time: 1270
    * average nodes: 195031
    * average cost: 2931
    * average depth reached: 22
* GD1 
> _fast, not optimal, high cpu, low ram_

    * execution time: 4226
    * Nodes expanded: 50642
    * total time: 14259
    * average excecution time: 1296
    * average nodes: 12742
    * average cost: 3455
    * average depth reached: 27
* GD2 
> _fast, not optimal, high cpu, low ram_

    * execution time: 3880
    * Nodes expanded: 50642
    * total time: 12216
    * average excecution time: 1110
    * average nodes: 12742
    * average cost: 3455
    * average depth reached: 27
* AS1
> _fast, optima, high cpu, low ram_

    * execution time: 3476
    * Nodes expanded: 44062
    * total time: 10370
    * average excecution time: 942
    * average nodes: 11122
    * average cost: 3455
    * average depth reached: 27
* AS2
> _fast, optimal, high cpu, low ram_

    * execution time: 4401
    * Nodes expanded: 50204
    * total time: 13156
    * average excecution time: 1196
    * average nodes: 12648
    * average cost: 3455
    * average depth reached: 27
