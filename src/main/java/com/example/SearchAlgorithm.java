package com.example;

import java.util.ArrayList;
import java.util.HashSet;

public abstract class SearchAlgorithm {
    protected HashSet<State> visited_states = new HashSet<State>();
    abstract Node search(ArrayList<Node> leave_nodes_at_the_moment); // retruns the index of the node that should be expanded next
    abstract int searchIndex(ArrayList<Node> leave_nodes_at_the_moment);
    abstract void enqueue(ArrayList<Node> expanded_nodes, Node new_node);
    public boolean visited(State state) {
        //We make sure this state is not a visited state cuz we don't want loops
        return this.visited_states.contains(state);
    }
    public void visit_state(State state) {
        this.visited_states.add(state);
    }
    public Node deque(ArrayList<Node> leaves_nodes) {
        return leaves_nodes.get(0); // usually we want to give the newst node
    }
}
