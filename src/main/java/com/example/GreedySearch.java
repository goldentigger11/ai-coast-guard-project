package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GreedySearch extends SearchAlgorithm {

    private Heurestic heurestic_calculator;

    GreedySearch() {
        heurestic_calculator = new DistanceToClosestShip();
    }
    GreedySearch(int heurestic_number){
        /*0 means use DistanceToClosestShip. 1: use CapacityMetric. 2: use Max of both heurstics */
        if(heurestic_number==1){
            heurestic_calculator = new CapacityMetric();
        } else if (heurestic_number==0){
            heurestic_calculator = new DistanceToClosestShip();
        }
        else if (heurestic_number==2){
            heurestic_calculator = new WeightedDistanceToClosestShip();
        }
        else {
           ArrayList<Heurestic> hs = new ArrayList<>();
           hs.add(new DistanceToClosestShip());
           hs.add(new CapacityMetric());
           heurestic_calculator = new MinHeurstic(hs);
        }
    }

    @Override
    Node search(ArrayList<Node> leave_nodes_at_the_moment) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    int searchIndex(ArrayList<Node> leave_nodes_at_the_moment) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    void enqueue(ArrayList<Node> expanded_nodes, Node new_node) {
        // add heurstic cost to new node
        new_node.heurestic = this.getHeursticCostOfNode(new_node);
        // sort them based on heurstic cost
        expanded_nodes.add(new_node);
        // after this we sort them based on heuristic of each node
        Collections.sort(expanded_nodes, new Comparator<Node>() {
            @Override
            public int compare(Node a1, Node a2) {
                return (int) (a1.heurestic - a2.heurestic);
            }
        });
    }

    private double getHeursticCostOfNode(Node new_node) {
        return this.heurestic_calculator.calculate(new_node);
    }

    private class MinHeurstic extends Heurestic{
        private ArrayList<Heurestic> hs;
        MinHeurstic(ArrayList<Heurestic> hs){
            this.hs = hs;
        }
        @Override
        public double calculate(Node n) {
            double min_h = 10000000;
            for (Heurestic heurestic : hs) {
                double heurstic_cost = heurestic.calculate(n);
                if(heurstic_cost<min_h){
                    min_h = heurstic_cost;
                }
            }
            System.out.println(min_h + " is the best heurstic at");
            return min_h;
        }
        
    }
}
