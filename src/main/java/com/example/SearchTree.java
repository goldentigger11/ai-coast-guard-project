package com.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import com.google.gson.Gson;
import com.rits.cloning.Cloner;

public class SearchTree {
    // CONFIG
    static boolean life_guard_must_return_to_station_at_the_end = true;

    Node root;
    public ArrayList<Node> leaves_nodes = new ArrayList<>();
    int init_capacity;
    int init_width;
    int init_height;

    SearchAlgorithm algorithm; // ? this is the algorithm the search tree will use for enque

    public SearchTree(Node starting_node) {
        root = starting_node;
        leaves_nodes.add(root);
        // save the inital capacity
        this.init_capacity = starting_node.state.capacity;
        this.init_width = starting_node.gird_width;
        this.init_height = starting_node.grid_height;
    }

    public void expandNode(Node n) {
        // use expand node index but get indx
        int indx = this.leaves_nodes.indexOf(n);
        this.expandNodeIndex(indx);
    }

    public ArrayList<Node> expandNodeIndex(int indx) {
        Node n = this.leaves_nodes.remove(indx);
        // to expand this node we 1 get all availible actions
        ArrayList<Action> availible_actions = n.getAvailaibleActions();
        ArrayList<Node> expanded_nodes = new ArrayList<>();
        for (Action action : availible_actions) {
            // add extra node to tree
            // node is the node after we expanded the tree
            Node node = n.deepClone();
            // Now we alter node based on what happened
            if (action == Action.Move_up) {
                node.prv_action = Action.Move_up;
                node.state.y_pos = node.state.y_pos - 1; // cuz we moved up // ? also we checked wheather this is
                // availible action already
            } else if (action == Action.Move_down) {
                node.prv_action = Action.Move_down;
                node.state.y_pos += 1;
            } else if (action == Action.Move_left) {
                node.prv_action = Action.Move_left;
                node.state.x_pos -= 1;
            } else if (action == Action.Move_right) {
                node.prv_action = Action.Move_right;
                node.state.x_pos += 1;
            } else if (action == Action.Drop) {
                node.prv_action = Action.Drop;
                // the agent just droped off the ppl and is ready to go back again
                node.state.capacity = this.init_capacity;

            } else if (action == Action.Pick_up) {
                node.prv_action = Action.Pick_up;
                // get the ship the agent is currently overlaping with
                ShipBody current_ship = null;
                for (ShipBody ship : node.state.ships) {
                    if (ship.x_pos == node.state.x_pos && ship.y_pos == node.state.y_pos) {
                        current_ship = ship;
                    }
                }

                // now the ship has a number of civilians that must be saved
                // take as many civilians as possible
                int new_capacity = current_ship.take_civilians(node.state.capacity, node.max_capacity);
                int number_of_ppl_saved = node.state.capacity - new_capacity;
                node.state.capacity = new_capacity;
                node.state.saved_ppl += number_of_ppl_saved;
                // if there are no more civilians then the ship will turn into a wreck
                // ? then it must move from the ships array to the wrecks array
                if (current_ship.type == Type.Wrek) {
                    // remove from ships arraylist
                    node.state.ships.remove(current_ship);
                    // add to wrecks arraylist
                    node.state.wrecks.add(current_ship);
                }

            } else if (action == Action.Retrieve) {
                node.prv_action = Action.Retrieve;
                // find the wreck that we overlaped with
                ShipBody current_wreck = null;
                for (ShipBody wreck : node.state.wrecks) {
                    if (wreck.x_pos == node.state.x_pos && wreck.y_pos == node.state.y_pos) {
                        current_wreck = wreck;
                    }
                }

                node.state.black_boxes += 1; // increase number of black boxes saved
                node.state.wrecks.remove(current_wreck); // remove this wreck from state as it is now nothing
            }

            // make the world clock tick as this node is a new node
            node.state.count();
            // Add this action as the prv_action
            // Add this node to the list of expanded nodes
            expanded_nodes.add(node);

        }

        // now we calculate properties for all the new expanded nodes
        for (Node node : expanded_nodes) {
            if (!this.algorithm.visited(node.state)) {
                node.prv_node = n; // make this n the parent node
                // TODO: Calculate herustic for this node
                // path cost here is time. so increas by 1 cuz we moved 1 step
                node.path_cost = CoastGuard.path_cost_calculator(node);
                node.depth = 1 + n.depth;
                // ? here we let the algorithm enque the node
                algorithm.enqueue(this.leaves_nodes, node);
                this.algorithm.visit_state(node.state);
            } else {
                // System.out.println("visited this node before");
            }
        }
        // Add these last nodes to the tree
        return expanded_nodes;
    }

    public ArrayList<Node> _fakeExpandatIndex(int indx) {
        Node n = this.leaves_nodes.remove(indx);
        // to expand this node we 1 get all availible actions
        ArrayList<Action> availible_actions = n.getAvailaibleActions();
        ArrayList<Node> expanded_nodes = new ArrayList<>();
        for (Action action : availible_actions) {
            // add extra node to tree
            // node is the node after we expanded the tree
            Node node = n.deepClone();
            // Now we alter node based on what happened
            // node.state.y_pos = node.state.y_pos-1; // cuz we moved up // ? also we
            // checked wheather this is availible action already
            // Add this action as the prv_action
            // Add this node to the list of expanded nodes
            expanded_nodes.add(node);
        }

        // now we calculate properties for all the new expanded nodes
        for (Node node : expanded_nodes) {
            node.prv_node = n; // make this n the parent node
            // TODO: Calculate herustic for this node
            node.heurestic = 0;
            // path cost here is time. so increas by 1 cuz we moved 1 step
            node.path_cost = 1 + n.path_cost;
            node.depth = 1 + n.depth;
        }
        return expanded_nodes;

    }

    public static boolean checkGoal(Node n) {
        // What makes the game end is if there are
        // 1 no ppl dying so everyone is on the guard ship or the stations
        // 2 there are no wrecks that still have a black box in them cuz black box can
        // die too
        State state = n.state;
        ArrayList<ShipBody> ships = state.ships;
        ArrayList<ShipBody> wrecks = state.wrecks;
        if (ships.isEmpty() && wrecks.isEmpty())
            if (life_guard_must_return_to_station_at_the_end) {
                // ? extra condition to make sure the lifegaurd returns to the ship again
                ArrayList<Station> stations = state.stations;
                for (Station station : stations) {
                    if (station.x_pos == state.x_pos && station.y_pos == state.y_pos) {
                        if(state.capacity==n.max_capacity){
                            // ?  if the agent droped off the last remaing ppl on the station
                            return true;
                        }
                    }
                }
            } else {
                return true;
            }

        return false;
    }

    public static void main(String[] args) {
        System.out.println("int state");
        // We will try to simulate a real problem
        // choose grid
        int grid_width = 3;
        int grid_height = 3;
        State inital_State;
        int x = 1;
        int y = 1;
        int max_capacity = 5;
        ArrayList<ShipBody> ships = new ArrayList<>();
        ships.add(new ShipBody(2, 0));
        ships.add(new ShipBody(2, 2));
        ArrayList<Station> stations = new ArrayList<>();
        stations.add(new Station(0, 0));
        inital_State = new State(x, y, max_capacity, ships, new ArrayList<ShipBody>(), stations);

        Node initial_node = Node.InitialNode(inital_State, grid_width, grid_height, max_capacity);
        Grid grid = new Grid(initial_node.state, initial_node.gird_width, initial_node.grid_height);

        // create s search tree
        SearchTree search_tree = new SearchTree(initial_node);
        SearchAlgorithm search_algo = new A_Star();
        search_tree.algorithm = search_algo;
        for (int i = 0; i < 1000; i++) {

            search_tree.print();
            // int indx = search_algo.searchIndex(search_tree.leaves_nodes);
            Node choosen_node = search_tree.algorithm.deque(search_tree.leaves_nodes);
            grid = new Grid(choosen_node.state, grid_width, grid_height);
            grid.Print();
            search_tree.expandNode(choosen_node);
            Node n = choosen_node;
            while (n.prv_node != null) {
                System.out.print(n.prv_action + " <--- ");
                n = n.prv_node;
            }
            System.out.println("");
            System.out.println("||||||||| STEP ||||||||" + " path cost "+choosen_node.path_cost+ " population " + choosen_node.getPopulationNow() + " depth " + choosen_node.depth);
            if (search_tree.checkGoal(choosen_node)) {
                System.out.println("GOOOOOOOOAAAAAAAAAAAALLLLLLLLLL in " + i + " iterations");

                System.out.println("||---------|| GAMEPLAY ||--------||");
                // grid show gameplay
                grid.show_gameplay(choosen_node);
                break;
            }
        }
    }

    void print() {
        if (leaves_nodes.isEmpty()) {
            System.out.print("Empty treee");
        }
        // debug the tree itself
        for (Node node : this.leaves_nodes) {
            System.out.print(node);
        }
        System.out.println("");
    }
}
