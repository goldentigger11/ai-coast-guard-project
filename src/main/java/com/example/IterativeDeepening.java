package com.example;

import java.util.ArrayList;
import java.util.HashSet;

public class IterativeDeepening extends SearchAlgorithm {
    public int max_depth = 1;
    private boolean initia_node_flage = true;
    private Node initial_node ;
    @Override
    Node search(ArrayList<Node> leave_nodes_at_the_moment) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    int searchIndex(ArrayList<Node> leave_nodes_at_the_moment) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    void enqueue(ArrayList<Node> expanded_nodes, Node new_node) {
        // if the new node has more depth than the max we won't enque it 
        if(new_node.depth > max_depth)
            return;
        // enque like in DFS
        expanded_nodes.add(0, new_node);
    }
    @Override
    public Node deque(ArrayList<Node> leaves_nodes) {
        // if this is the 1st time we enque a new node then it is the inital node. 
        // save the inital node as we will need it to restart 
        if(this.initia_node_flage){
            Node first_node = leaves_nodes.get(0);
            this.initial_node = first_node.deepClone();
            this.initia_node_flage = false; // never come back here again
        }
        // when we deque a leaf node. 
        // if there are no more leave nodes than start all over again
        if(leaves_nodes.isEmpty()){
            // System.out.println("Max Depth + 1");
            this.max_depth +=1;
            // we will restart the visited states
            this.visited_states = new HashSet<>();
            // then we will insert the inital state again 
            leaves_nodes.add(initial_node);
            return this.initial_node;
        } else {
            return leaves_nodes.get(0);
        }
    }
    

}
