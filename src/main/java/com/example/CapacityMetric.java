package com.example;

import java.util.ArrayList;

public class CapacityMetric extends Heurestic {

    @Override
    public double calculate(Node n) {
        // 2 states for the heurstic
        // when we still have capacity
        // we try to choose action that will make us pick up ppl
        double heurestic = 1000000; // for the rest of the actions
        //
        int max_capacity = n.max_capacity;
        int current_capacity = n.state.capacity;
        Action action = n.prv_action;
        // if (current_capacity > 0) {
        if (action == Action.Pick_up) {
            heurestic = -10;
        }
        // } else {

        // when we do not have capacity
        // we choose action to dispose of ppl
        if (action == Action.Drop) {
            heurestic = -100;
        }
        // }
        return heurestic;
    }

    public static void main(String[] args) {
        State inital_State;
        int x = 1;
        int y = 0;
        int capacity = 5;
        ArrayList<ShipBody> ships = new ArrayList<>();
        ships.add(new ShipBody(1, 0));
        ArrayList<Station> stations = new ArrayList<>();
        stations.add(new Station(0, 0));
        inital_State = new State(x, y, capacity, ships, new ArrayList<ShipBody>(), stations);

        int grid_height = 2;
        int grid_width = 2;
        Node initial_node = new Node(inital_State, null, null);
        initial_node.gird_width = grid_width;
        initial_node.grid_height = grid_height;
        initial_node.max_capacity = capacity;
        Grid grid = new Grid(inital_State, initial_node.gird_width, initial_node.grid_height);
        grid.Print();
        CapacityMetric c = new CapacityMetric();
        initial_node.prv_action = Action.Pick_up;
        System.out.println(c.calculate(initial_node));

    }

}
