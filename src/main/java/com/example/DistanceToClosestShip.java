package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.lang.Math;;

public class DistanceToClosestShip extends Heurestic {
    // the cost here will be distance to closest ship
    @Override
    public double calculate(Node n) {
        // Centering
        if(SearchTree.checkGoal(n)){
            return 0;
        }

        // player posistion
        int x_pos = n.state.x_pos;
        int y_pos = n.state.y_pos;
        ArrayList<Double> distances = new ArrayList<>();

        // calculate distance to closest ship or wreck if there are no ships
        if (!n.state.ships.isEmpty()) {
            // if there are some ships
            ArrayList<ShipBody> ships = n.state.ships;
            for (ShipBody ship : ships) {
                // distance between me and ship is eucledian
                float d_x = x_pos - ship.x_pos;
                float d_y = y_pos - ship.y_pos;
                double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                distances.add(distance);
            }
        } else if (!n.state.wrecks.isEmpty()){
            // if there are no ships
            for (ShipBody wereck : n.state.wrecks) {
                float d_x = x_pos - wereck.x_pos;
                float d_y = y_pos - wereck.y_pos;
                double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                distances.add(distance);
            }
        } else {
            
        // in case there are no wrecks also then we get distance to the closests station 
        for (Station station : n.state.stations) {
            float d_x = x_pos - station.x_pos;
            float d_y = y_pos - station.y_pos;
            double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
            distances.add(distance);
        }
        }
       
        // get the smallest distance and give it
        return Collections.min(distances);
    }

}
