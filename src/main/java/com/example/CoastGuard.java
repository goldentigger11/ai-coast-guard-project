package com.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CoastGuard {

    static Node final_node = null;

    static double path_cost_calculator(Node node) {
        // a good path cost is the total population now vs in the start of the game
        double time_cost = node.depth;
        double black_boxes_cost = -node.state.black_boxes;
        double ppl_cost2 = node.getPplThatDiedNow();
        double prv_cost = node.prv_node.path_cost;

        double cost = prv_cost + 100 * ppl_cost2 + black_boxes_cost + time_cost;
        return cost;
    }

    // the class the tests will run on
    public static String solve(String str_grid, String strategy, boolean visualise) {
        // INITALIZE GRID
        // PARSING
        StringTokenizer semi_cols = new StringTokenizer(str_grid, ";");
        String init_grid_str = semi_cols.nextToken();
        String capacity_str = semi_cols.nextToken();
        String starting_pos_str = semi_cols.nextToken();
        String stations_str = semi_cols.nextToken();
        String ships_str = semi_cols.nextToken();
        //
        // PUTTING IN VARIABLES
        State inital_State; // ? initial state that we will parse the stirng into
        StringTokenizer comas = new StringTokenizer(init_grid_str, ",");
        // ! either keep it like this or swithc it such that when we add something we
        // parse the width 1st then height
        // ! ask TA
        int grid_width = Integer.parseInt(comas.nextToken());
        int grid_height = Integer.parseInt(comas.nextToken()); // ! this is wrong cuz it should be height that width
        comas = new StringTokenizer(starting_pos_str, ",");
        int y = Integer.parseInt(comas.nextToken());
        int x = Integer.parseInt(comas.nextToken());
        comas = new StringTokenizer(capacity_str, ",");
        int max_capacity = Integer.parseInt(comas.nextToken());

        // Now parse ships into ships array
        ArrayList<ShipBody> ships = new ArrayList<>();
        comas = new StringTokenizer(ships_str, ",");
        while (comas.hasMoreTokens()) {
            int y_pos = Integer.parseInt(comas.nextToken());
            int x_pos = Integer.parseInt(comas.nextToken());
            int civilians = Integer.parseInt(comas.nextToken());
            ships.add(new ShipBody(x_pos, y_pos, civilians));
        }

        // PARSE THE STATIONS
        ArrayList<Station> stations = new ArrayList<>();
        comas = new StringTokenizer(stations_str, ",");
        while (comas.hasMoreTokens()) {
            int y_pos = Integer.parseInt(comas.nextToken());
            int x_pos = Integer.parseInt(comas.nextToken());
            stations.add(new Station(x_pos, y_pos));
        }

        inital_State = new State(x, y, max_capacity, ships, new ArrayList<ShipBody>(), stations);

        Node initial_node = Node.InitialNode(inital_State, grid_width, grid_height, max_capacity);
        Grid grid = new Grid(initial_node.state, initial_node.gird_width, initial_node.grid_height);
        if (visualise)
            grid.Print();

        // Now make sure algorithm is ready
        // create s search tree
        SearchAlgorithm search_algo = new A_Star();
        SearchTree search_tree = new SearchTree(initial_node);
        switch (strategy) {
            case "BF":
                search_algo = new BredthFirstSearch();
                break;
            case "DF":
                search_algo = new DepthFirstSearch();
                break;
            case "ID":
                search_algo = new IterativeDeepening();
                break;
            case "GR1":
                search_algo = new GreedySearch(0);
                break;
            case "GR2":
                search_algo = new GreedySearch(1);
                break;
            case "AS1":
                search_algo = new A_Star(1);
                break;
            case "AS2":
                search_algo = new A_Star(2);
                break;

        }
        search_tree.algorithm = search_algo;

        for (int i = 0; i < 10000000; i++) {

            // search_tree.print();
            // int indx = search_algo.searchIndex(search_tree.leaves_nodes);
            Node choosen_node = search_tree.algorithm.deque(search_tree.leaves_nodes);
            grid = new Grid(choosen_node.state, grid_width, grid_height);
            Node n = choosen_node;
            if (visualise) {
                grid.Print();
                while (n.prv_node != null) {
                    System.out.print(n.prv_action + " <--- ");
                    n = n.prv_node;
                }
                System.out.println("");
                System.out.println("||||||||| STEP ||||||||" + " path cost " + choosen_node.path_cost + " population "
                        + choosen_node.getPopulationNow() + " depth " + choosen_node.depth);
            }
            search_tree.expandNode(choosen_node);
            if (search_tree.checkGoal(choosen_node)) {
                final_node = choosen_node;
                Node goal_node = choosen_node;
                // Extract answers
                int nodes_for_expansions = i; // ? number of times we expanded nodes to get here
                int deaths = Node.inital_population - goal_node.state.saved_ppl;
                int retrived = goal_node.state.black_boxes;
                String plan = "";
                ArrayList<Node> actions = goal_node.getPathNodes();
                for (Node node : actions) {
                    plan = plan + "," + node.prv_action;
                }
                if (visualise) {
                    System.out.println("GOOOOOOOOAAAAAAAAAAAALLLLLLLLLL in " + i + " iterations");
                    System.out.println("||---------|| GAMEPLAY ||--------||");
                    // grid show gameplay
                    grid.show_gameplay(final_node);
                }
                // FINALY RETURN

                String ans = plan + ";" + deaths + ";" + retrived + ";" + nodes_for_expansions;
                return format_answer_3ashan_el_test_code_3ayota_begad_a7a(ans);
                // return ans;
            }
        }

        return "COULD NOT FIND A PATH SORRY::";

    }

    private static String format_answer_3ashan_el_test_code_3ayota_begad_a7a(String ans) {
        // mabda2eyan sheel 2awel action 3ashan null
        ans = ans.substring(6);
        // for each action zabat el print beta3to
        StringTokenizer semi = new StringTokenizer(ans, ";");
        String plan = semi.nextToken();
        StringTokenizer comas = new StringTokenizer(plan, ",");
        String answer_2 = "";
        while (comas.hasMoreTokens()) {
            String word = comas.nextToken();
            switch (word) {
                case "Move_left":
                    answer_2 += "left,";
                    break;
                case "Move_up":
                    answer_2 += "up,";
                    break;
                case "Move_down":
                    answer_2 += "down,";
                    break;
                case "Move_right":
                    answer_2 += "right,";
                    break;
                case "Pick_up":
                    answer_2 += "pickup,";
                    break;
                case "Drop":
                    answer_2 += "drop,";
                    break;
                case "Retrieve":
                    answer_2 += "retrieve,";
                    break;
                default:
                    answer_2 += word;
            }
        }
        return answer_2 + ";" + semi.nextToken() + ";" + semi.nextToken() + ";" + semi.nextToken();
    }

    public static void main(String[] args) {
        ArrayList<String> grids = new ArrayList<>();
        String grid0 = "5,6;50;0,1;0,4,3,3;1,1,90;";grids.add(grid0);
        String grid1 = "6,6;52;2,0;2,4,4,0,5,4;2,1,19,4,2,6,5,0,8;";
        grids.add(grid0);
        String grid2 = "7,5;40;2,3;3,6;1,1,10,4,5,90;";
        grids.add(grid1);
        String grid3 = "8,5;60;4,6;2,7;3,4,37,3,5,93,4,0,40;";
        grids.add(grid2);
        String grid5 = "5,5;69;3,3;0,0,0,1,1,0;0,3,78,1,2,2,1,3,14,4,4,9;";
        grids.add(grid3);
        String grid6 = "7,5;86;0,0;1,3,1,5,4,2;1,1,42,2,5,99,3,5,89;";
        grids.add(grid5);
        String grid7 = "6,7;82;1,4;2,3;1,1,58,3,0,58,4,2,72;";
        grids.add(grid6);
        String grid8 = "6,6;74;1,1;0,3,1,0,2,0,2,4,4,0,4,2,5,0;0,0,78,3,3,5,4,3,40;";
        grids.add(grid7);
        String grid9 = "7,5;100;3,4;2,6,3,5;0,0,4,0,1,8,1,4,77,1,5,1,1,6,55,3,2,94,4,3,46;";
        grids.add(grid8);
        String grid10 = "10,6;59;1,7;0,0,2,2,3,0,5,3;1,3,69,3,4,80,4,7,94,4,9,14,5,2,39;";
        grids.add(grid9);
        grids.add(grid10);
        String gridHard = "5,3;33;0,4;0,4,1,4,2,2,2,1,1,3,0,3;1,0,62,2,0,74,0,0,99,1,2,57,2,4,37,2,3,89;";

        long sum_time = 0;

        long sum_nodes = 0;
        long sum_cost = 0;
        long sum_dpeth = 0;
        for (String gString : grids) {
            String grid = grid10;
            final long startTime = System.currentTimeMillis();
            String ans = CoastGuard.solve(grid, "GR1", false);
            System.out.println(ans);
            String[] t = ans.split(";");
            long nodes = Long.parseLong(t[t.length-1]);


            Grid.show_gameplay(final_node);
            System.out.println(Checker.applyPlan(grid, ans));
            final long endTime = System.currentTimeMillis();
            System.out.println("execution time: " + (endTime - startTime));
            long time = endTime - startTime;
            System.out.println("Nodes expanded: " + nodes);
            sum_time += time;
            sum_nodes += nodes;
            sum_cost += final_node.path_cost;
            sum_dpeth += final_node.depth;
            break;
        }
        System.out.println("total time: " + sum_time);
        System.out.print("average excecution time: ");
        System.out.println(sum_time/grids.size());
        System.out.print("average nodes: ");
        System.out.println(sum_nodes/grids.size());
        System.out.print("average cost: ");
        System.out.println(sum_cost/grids.size());
        System.out.print("average depth reached: ");
        System.out.println(sum_dpeth/grids.size());

    }

}
