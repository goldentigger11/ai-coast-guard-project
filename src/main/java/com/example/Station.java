package com.example;

import java.util.Objects;

public class Station extends GameObj {

    public int x_pos;
    public int y_pos;
    public Station(int _xpos, int _ypos){
        x_pos = _xpos;
        y_pos = _ypos;
    }
    @Override
    public void count() {
        // TODO Auto-generated method stub
        
    }
    @Override
    public boolean equals(Object obj) {
        Station s2 = (Station) obj;
        return (
            this.x_pos == s2.x_pos &&
            this.y_pos == s2.y_pos
        );
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.x_pos, this.y_pos, this.type);
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "^^^^";
    }
}
