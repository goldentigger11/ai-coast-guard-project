package com.example;

import java.util.ArrayList;
import java.util.Collections;

public class WeightedDistanceToClosestShip  extends Heurestic{

    @Override
    public double calculate(Node n) {
        // player posistion
        int x_pos = n.state.x_pos;
        int y_pos = n.state.y_pos;
        ArrayList<Double> distances = new ArrayList<>();

        // calculate distance to closest ship or wreck if there are no ships
        if (!n.state.ships.isEmpty()) {
            // if there are some ships
            ArrayList<ShipBody> ships = n.state.ships;
            for (ShipBody ship : ships) {
                // distance between me and ship is eucledian
                float d_x = x_pos - ship.x_pos;
                float d_y = y_pos - ship.y_pos;
                double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                double distance_times_number_of_ppl = distance*ship.civilians;
                distances.add(distance_times_number_of_ppl);
            }
        } else if (!n.state.wrecks.isEmpty()) {
            // if there are no ships
            for (ShipBody wereck : n.state.wrecks) {
                float d_x = x_pos - wereck.x_pos;
                float d_y = y_pos - wereck.y_pos;
                double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                double distance_times_number_of_ppl = distance*wereck.black_box_health;
                distances.add(distance_times_number_of_ppl);
            }
        } else {

            // in case there are no wrecks also then we get distance to the closests station
            for (Station station : n.state.stations) {
                float d_x = x_pos - station.x_pos;
                float d_y = y_pos - station.y_pos;
                double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                distances.add(distance);
            }
        }

        // get the smallest distance and give it
        return Collections.min(distances);
    }
    
}
