package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DistanceToHealthiestShip extends Heurestic {

    @Override
    public double calculate(Node n) {
        if(SearchTree.checkGoal(n)){
            return 0;
        }
        State s = n.state;
        if (s.capacity > 0 && !n.state.ships.isEmpty()) {
            // The heurstic is to minimze the distnace to ship with most ppl
            ShipBody best = Collections.min(n.state.ships, new Comparator<ShipBody>() {
                @Override
                public int compare(ShipBody o1, ShipBody o2) {
                    return o1.civilians - o2.civilians;
                }
            });

            float d_x = s.x_pos - best.x_pos;
            float d_y = s.y_pos - best.y_pos;
            double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
            return distance;
        } else {
            if (n.state.ships.isEmpty() && !n.state.wrecks.isEmpty()) {
                // no more ships look for balack boxes with least health
                ShipBody best = Collections.min(n.state.wrecks, new Comparator<ShipBody>() {
                    @Override
                    public int compare(ShipBody o1, ShipBody o2) {
                        return o1.black_box_health - o2.black_box_health;
                    }
                });

                float d_x = s.x_pos - best.x_pos;
                float d_y = s.y_pos - best.y_pos;
                double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                return distance;

            } else {
                ArrayList<Double> distances = new ArrayList<>();
                for (Station station : n.state.stations) {
                    float d_x = s.x_pos - station.x_pos;
                    float d_y = s.y_pos - station.y_pos;
                    double distance = Math.sqrt(Math.pow(d_x, 2) + Math.pow(d_y, 2));
                    distances.add(distance);
                }
                return Collections.min(distances);
            }
        }

    }

}
