package com.example;

public abstract class GameObj {
   public Type type; 
   abstract public void count();
   abstract public String toString();
}
