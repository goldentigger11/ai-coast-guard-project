package com.example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rits.cloning.Cloner;

import java.util.ArrayList;

public class Node {
   State state;
   Node prv_node;
   Action prv_action;
   double path_cost;
   double heurestic;
   int depth = 0;

   // optimizations
   int population_now = -1;
   // init variables that tell the node how to world is
   static int gird_width;
   static int grid_height;
   static int max_capacity;
   static int inital_population;
   static int inital_number_of_balck_boxes = 0;

   private static Cloner cloner = new Cloner();

   public static Node InitialNode(State _inital_state, int _grid_width, int _grid_height, int _max_capacity) {
      gird_width = _grid_width;
      grid_height = _grid_height;
      max_capacity = _max_capacity;

      int total_pop = 0;

      // calcualte inital population
      for (ShipBody ship : _inital_state.ships) {
         total_pop += ship.civilians;
         inital_number_of_balck_boxes += 1;
      }
      inital_population = total_pop;

      return new Node(_inital_state, null, null);
   }

   public Node(State _state, Node _prv_node, Action _prv_action) {
      state = _state;
      prv_node = _prv_node;
      prv_action = _prv_action;
   }

   public ArrayList<Action> getAvailaibleActions() {
      // get agent pos x,y. Assume you started at initial position 0,0
      // check surrounding nodes and store this info
      // Take a decision to move to one of these nodes
      // check what are the possible actions to do with at the current node
      int curX = state.x_pos;
      int curY = state.y_pos;
      int current_capacity = state.capacity;
      int max_cap = this.max_capacity;
      ArrayList<Action> availableActions = new ArrayList<>();
      if (curX < gird_width - 1)
         availableActions.add(Action.Move_right);
      if (curX > 0)
         availableActions.add(Action.Move_left);
      if (curY < grid_height - 1)
         availableActions.add(Action.Move_down);
      if (curY > 0)
         availableActions.add(Action.Move_up);
      for (ShipBody ship : state.ships) {
         if (ship.x_pos == curX && ship.y_pos == curY && current_capacity > 0) {
            availableActions.add(Action.Pick_up);
         }
      }
      for (ShipBody wreck : state.wrecks) {
         if (wreck.x_pos == curX && wreck.y_pos == curY) {
            availableActions.add(Action.Retrieve);
         }
      }
      for (Station station : state.stations) {
         if (station.x_pos == curX && station.y_pos == curY && state.capacity < this.max_capacity)
            availableActions.add(Action.Drop);
      }
      return availableActions;

   }

   public Node deepClone2() {
      Node clone = cloner.deepClone(this);
      // clone is a deep-clone of o
      return clone;
   }

   public Node deepClone() {
      State clone_state = cloner.deepClone(this.state);

      Node clone = new Node(clone_state, prv_node, prv_action);
      clone.prv_action = this.prv_action;
      clone.path_cost = this.path_cost;
      clone.heurestic = this.heurestic;
      clone.depth = this.depth;
      return clone;
   }

   // Helpers
   public int getDepth() {
      return this.depth;
   }

   @Override
   public String toString() {
      return "Node(" + this.depth + ")";
   }

   public int getPopulationNow() {
      if (population_now == -1) {
         int pop = 0;
         for (ShipBody shipBody : this.state.ships) {
            pop += shipBody.civilians;
         }
         population_now = pop;

         return pop;
      } 
      return population_now;
   }

   public int getPplThatDiedNow() {
      return this.state.ships.size();
   }

   public int getBlackBoxesNow() {
      return this.state.ships.size() + this.state.wrecks.size();
   }

   public ArrayList getPathNodes() {
      Node prv = this;
      ArrayList<Node> path = new ArrayList<>();
      while (prv != null) {
         path.add(0, prv);
         prv = prv.prv_node;
      }
      return path;
   }

   public static void main(String[] args) {

   }
}
