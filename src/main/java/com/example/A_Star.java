package com.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class A_Star extends SearchAlgorithm {

    private Heurestic addmissable_heurstic = new DistanceToClosestShip();

    public A_Star(int i) {
        if (i == 1) {
            addmissable_heurstic = new DistanceToClosestShip();
        } else if (i == 2) {
            addmissable_heurstic = new DistanceToHealthiestShip();
        } else {
            addmissable_heurstic = new hs();
        }
    }

    public A_Star() {
        this(0);
    }

    @Override
    Node search(ArrayList<Node> leave_nodes_at_the_moment) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    int searchIndex(ArrayList<Node> leave_nodes_at_the_moment) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    void enqueue(ArrayList<Node> expanded_nodes, Node new_node) {
        // choose based on both the admisable heurstic as well as the path cost we keda
        // get heurstic cost

        double heurestic_cost = this.addmissable_heurstic.calculate(new_node);
        new_node.heurestic = heurestic_cost;
        
        // add it to the expanded nodes
        expanded_nodes.add(new_node);

        // sort em up
        Collections.sort(expanded_nodes, new Comparator<Node>() {
            @Override
            public int compare(Node a1, Node a2) {
                return (int) ((a1.heurestic + a1.path_cost) - (a2.heurestic + a2.path_cost));
            }
        });

    }

    

    class hs extends Heurestic {
        // This is a
        @Override
        public double calculate(Node n) {
            return 1;
        }

    }
}
