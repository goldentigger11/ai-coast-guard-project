package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class State {
    // Class representing the current state of the game
    int x_pos;
    int y_pos;
    int capacity;
    int black_boxes;
    int saved_ppl=0;
    ArrayList<ShipBody> ships = new ArrayList<>();
    ArrayList<ShipBody> wrecks = new ArrayList<>();
    ArrayList<Station> stations = new ArrayList<>();

    @Override
    public int hashCode() {

        return Objects.hash(this.x_pos, this.y_pos, capacity, black_boxes, this.ships, this.wrecks, this.stations);
    }

    @Override
    public boolean equals(Object obj) {
        //
        State s2 = (State) obj;
        boolean check = this.ships.equals(s2.ships);
        // for 2 states to be equal the must have the same everything
        if (this.x_pos == s2.x_pos &&
                this.y_pos == s2.y_pos &&
                this.capacity == s2.capacity &&
                this.black_boxes == s2.black_boxes &&
                this.ships.equals(s2.ships) &&
                this.wrecks.equals(s2.wrecks) &&
                this.stations.equals(s2.stations)) {
            return true;
        }
        return false;
    }

    public State(int _x_pos, int _y_pos, int _capacity, ArrayList<ShipBody> _ships, ArrayList<ShipBody> _wrecks,
            ArrayList<Station> _stations) {
        x_pos = _x_pos;
        y_pos = _y_pos;
        capacity = _capacity;
        ships = _ships;
        wrecks = _wrecks;
        stations = _stations;
    }

    public State() {
    }

    public static void main(String[] args) {

        int grid_width = 1;
        int grid_height = 3;
        State inital_State;
        int x = 0;
        int y = 1;
        int capacity = 20;
        ArrayList<ShipBody> ships = new ArrayList<>();
        ships.add(new ShipBody(0, 1));
        ArrayList<Station> stations = new ArrayList<>();
        stations.add(new Station(0, 0));
        inital_State = new State(x, y, capacity, ships, new ArrayList<ShipBody>(), stations);
        Grid grid = new Grid(inital_State, grid_width, grid_height);
        grid.Print();

        System.out.println(inital_State.hashCode());

        State inital_State2;
        ArrayList<ShipBody> ships2 = new ArrayList<>();
        ships2.add(new ShipBody(0, 1));
        ships2.get(0).type = Type.Wrek;
        ArrayList<Station> stations2 = new ArrayList<>();
        stations2.add(new Station(0, 0));

        ArrayList<ShipBody> wrecks = new ArrayList<>();
        ShipBody wreck = new ShipBody(0, 0, 1);
        inital_State2 = new State(x, y, capacity, ships2, wrecks, stations2);
        grid = new Grid(inital_State, grid_width, grid_height);
        grid.Print();
        System.out.println(inital_State2.hashCode());
        System.out.println(inital_State.equals(inital_State2));
    }

    public void count() {
        // for every gameObj in the state it will count
        // ships
        for (Iterator<ShipBody> i = this.ships.iterator(); i.hasNext();) {
            ShipBody ship = i.next();
            // Do Something
            ship.count();
            // if the ship no longer has anu surviors then we must make it into a wreck
            if (ship.civilians == 0) {
                i.remove();
                ship.type = Type.Wrek;
                this.wrecks.add(ship);
            }
        }
        // count for wrecks
        for (Iterator<ShipBody> i = this.wrecks.iterator(); i.hasNext();) {
            ShipBody wreck = i.next();
            // Do Something
            wreck.count();
            // if the ship no longer has anu surviors then we must make it into a wreck
            if (wreck.black_box_health == 0) {
                i.remove();
            }
        }

    }

}
